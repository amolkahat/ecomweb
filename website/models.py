from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Product(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    image = models.CharField(max_length=100)
    quantity = models.IntegerField()
    description = models.CharField(max_length=200)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.__repr__()


class Orders(models.Model):
    cust = models.ForeignKey(User, on_delete=models.CASCADE)
    prod = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.cust_id} {self.prod_id}"
