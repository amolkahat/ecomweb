from django.shortcuts import render, redirect
from .models import Product, Orders
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
# Create your views here.


def index(request):
    d = Product.objects.all()
    return render(request, 'index.html', {'product': d})


def user_login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            d = Product.objects.all()
            return render(request, 'index.html', {'product': d})
    return render(request, 'login.html')


def user_logout(request):
    logout(request)
    return render(request, 'login.html')


def signup(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password1')
        password2 = request.POST.get('password2')
        email = request.POST.get('email')
        if password == password2:
            u = User()
            u.email = email
            u.set_password(password)
            u.username = username
            u.save()
            return render(request, 'login.html')

    return render(request, 'signup.html')


def show_orders(request):
    if request.user.is_authenticated:
        o = Orders.objects.filter(cust=request.user)
        return render(request, 'orders.html', {'orders': o})
    else:
        return redirect('login')


def place_order(request, prod_id):
    if request.user.is_authenticated:

        p = Product.objects.get(id=prod_id)
        o = Orders(cust=request.user, prod=p)
        o.save()
        return redirect('show_orders')
    else:
        return redirect('login')
